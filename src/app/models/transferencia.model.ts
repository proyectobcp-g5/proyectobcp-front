export class Transferencia {
    cod_tran: number;
    monto: number;
    cod_destino_cuenta: number;
    descripcion: string;
    fecha: Date;
    hora: string;
    codCuenta: any;
    cod_tipo: any;
}