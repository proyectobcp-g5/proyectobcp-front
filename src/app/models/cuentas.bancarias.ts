export class CuentaBancaria {
    cod_cuenta: number;
    numCuenta: string;
    saldo: number;
    tarjeta: string;
    cod_tipo: any;
    codCli: any;
}