export class Notificacion {
    titulo: string;
    estado: number;
    descripcion: string;
    fecha: Date;
    hora: string;
    cod_tipo: any;
}