export interface cliente {
    cod_cli: number;
    nom_cli: string;
    apepat_cli: string;
    apemat_cli: string;
    dni: number;
    correo: string;
    password: string;
}